package com.nmgtechnologies.mvvm.util;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.nmgtechnologies.mvvm.data.model.api.User;
import com.nmgtechnologies.mvvm.ui.UserListAdapter;

import java.util.List;

public final class BindingUtils {


    @BindingAdapter({"adapter"})
    public static void addBlogItems(RecyclerView recyclerView, List<User> users) {
        UserListAdapter adapter = (UserListAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addItems(users);
        }
    }

}
