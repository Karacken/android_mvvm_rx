package com.nmgtechnologies.mvvm.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.nmgtechnologies.mvvm.BR;
import com.nmgtechnologies.mvvm.R;
import com.nmgtechnologies.mvvm.databinding.ActivityMainBinding;
import com.nmgtechnologies.mvvm.ui.base.BaseActivity;
import com.nmgtechnologies.mvvm.util.rx.AppSchedulerProvider;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import io.reactivex.Observable;

public class MainActivity extends BaseActivity<ActivityMainBinding,MainActivityViewModel> implements MainNavigator {

    private MainActivityViewModel mainActivityViewModel;
    private ActivityMainBinding mainActivityBinding;

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    UserListAdapter userListAdapter;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    RecyclerView.ItemAnimator itemAnimator;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public MainActivityViewModel getViewModel() {
        mainActivityViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel.class);
        return mainActivityViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivityBinding =getViewDataBinding();
        mainActivityViewModel.setNavigator(this);
        setUp();
    }

    private void setUp() {

        mainActivityBinding.setViewModel(mainActivityViewModel);
        mainActivityBinding.recyclerView.setLayoutManager(linearLayoutManager);
        mainActivityBinding.recyclerView.setItemAnimator(itemAnimator);
        mainActivityBinding.recyclerView.setAdapter(userListAdapter);
        subscribeToLiveData();
    }

    private void test() {
        final List<String> items = Arrays.asList("a", "b", "c", "d", "e", "f");

        Map<String, Integer> itemMap = new LinkedHashMap<>();
        itemMap.put("a", 5);
        itemMap.put("b", 1);
        itemMap.put("c", 5);
        itemMap.put("d", 10);
        itemMap.put("e", 5);
        itemMap.put("f", 8);
        final AppSchedulerProvider scheduler = new AppSchedulerProvider();
        Observable.fromIterable(itemMap.keySet())
                .switchMap(s -> Observable.just(s + "x").delay(itemMap.get(s), TimeUnit.SECONDS))
//                .flatMap(s->s)
                .doOnNext(s -> Log.d("RXJAVA", s + " " + System.currentTimeMillis()))
                .toList()
                .doOnSuccess(list -> Log.d("RXJAVA", list.toString()))
                .doOnError(list -> Log.d("RXJAVA", "on doOnError"))
                .doFinally(() -> Log.d("RXJAVA", "on doFinally"))
                .doOnDispose(() -> Log.d("RXJAVA", "on doOnDispose"))
                .observeOn(scheduler.ui())
                .subscribeOn(scheduler.io())
                .subscribe();

    }

    private void subscribeToLiveData() {
        mainActivityViewModel.getUserListLiveData().observe(this, users -> mainActivityViewModel.addUserObservableItems(users));
    }

    @Override
    public void openDetailActivity() {

    }
}
