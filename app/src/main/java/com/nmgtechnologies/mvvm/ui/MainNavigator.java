package com.nmgtechnologies.mvvm.ui;

public interface MainNavigator {
    void openDetailActivity();
}
