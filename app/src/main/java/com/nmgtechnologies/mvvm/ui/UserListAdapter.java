package com.nmgtechnologies.mvvm.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.nmgtechnologies.mvvm.data.model.api.User;
import com.nmgtechnologies.mvvm.databinding.UserItemBinding;
import com.nmgtechnologies.mvvm.ui.base.BaseRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;


public class UserListAdapter extends BaseRecyclerAdapter<User,UserItemViewModel,UserListAdapter.UserItemHolder> {
private List<UserItemViewModel> userViewModelList;

    public UserListAdapter(List<User> userList)
    {
        this.userViewModelList=toViewModelList(userList);
    }

    @NonNull
    @Override
    public UserItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new UserItemHolder(UserItemBinding.inflate(LayoutInflater.from(viewGroup.getContext()),viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserItemHolder userItemHolder, int i) {
        userItemHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        return userViewModelList.size();
    }

    public void clearItems() {
        this.userViewModelList.clear();
    }
    public void addItems(List<User> userList) {
        if(userList!=null)
        {
            this.userViewModelList.addAll(toViewModelList(userList));
            notifyDataSetChanged();
        }
    }

    @Override
    public List<UserItemViewModel> toViewModelList(List<User> objectList) {
        List<UserItemViewModel> userViewModelList = new ArrayList<>();
        for(User object : objectList)
            {
                userViewModelList.add(new UserItemViewModel(object));
            }

        return userViewModelList;
    }

    class UserItemHolder extends RecyclerView.ViewHolder
    {
        private UserItemBinding binding;
        public UserItemHolder(UserItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
        public void bind(int position)
        {
            this.binding.setViewModel(userViewModelList.get(position));
            this.binding.executePendingBindings();
        }
    }

}
