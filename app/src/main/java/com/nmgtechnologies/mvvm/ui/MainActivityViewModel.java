package com.nmgtechnologies.mvvm.ui;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import com.nmgtechnologies.mvvm.data.DataManager;
import com.nmgtechnologies.mvvm.data.model.api.User;
import com.nmgtechnologies.mvvm.data.model.api.GenericResponse;
import com.nmgtechnologies.mvvm.ui.base.BaseViewModel;
import com.nmgtechnologies.mvvm.util.AppConstants;
import com.nmgtechnologies.mvvm.util.rx.SchedulerProvider;

import java.util.List;

public class MainActivityViewModel extends BaseViewModel<MainNavigator> {
    ObservableList<User> userObservableList = new ObservableArrayList<>();
    MutableLiveData<List<User>> userListLiveData;

    public  MainActivityViewModel(DataManager dataManager, SchedulerProvider schedulerProvider)
    {
        super(dataManager,schedulerProvider);
        userListLiveData=new MutableLiveData<>();
        fetchUsers();
    }

    private void fetchUsers() {
        setLoading(true);
         getCompositeDisposable().add(getDataManager().getUserList()
        .subscribeOn(getSchedulerProvider().io())
        .observeOn(getSchedulerProvider().ui())
        .subscribe(this::handleUserListFetchSuccess,this::handleUserListFetchFailure)
        );
    }

    private void handleUserListFetchSuccess(GenericResponse<User> response) {
        setLoading(false);
        if (response.getCode()==AppConstants.API_CODE.SUCCESS)
        {
            userListLiveData.setValue(response.getData());

        }
    }

    private void handleUserListFetchFailure(Throwable throwable) {
        setLoading(false);
    }

    public void  addUserObservableItems(List<User> userList)
    {
        userObservableList.clear();
        userObservableList.addAll(userList);
    }

    public MutableLiveData<List<User>> getUserListLiveData() {
        return userListLiveData;
    }

    public ObservableList<User> getUserObservableList() {
        return userObservableList;
    }
}
