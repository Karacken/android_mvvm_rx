package com.nmgtechnologies.mvvm.ui.base;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.nmgtechnologies.mvvm.data.DataManager;
import com.nmgtechnologies.mvvm.util.rx.SchedulerProvider;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel<N> extends ViewModel {
    private final DataManager dataManager;
    private final SchedulerProvider schedulerProvider;
    private CompositeDisposable compositeDisposable;
    private WeakReference<N> navigator;
    private ObservableField<Boolean> isLoading = new ObservableField<>(false);
    public BaseViewModel(DataManager dataManager,SchedulerProvider schedulerProvider)
    {
        this.dataManager=dataManager;
        this.schedulerProvider=schedulerProvider;
        this.compositeDisposable = new CompositeDisposable();
    }
    public N getNavigator() {
        return navigator.get();
    }

    public void setNavigator(N navigator) {
        this.navigator = new WeakReference<>(navigator);
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public SchedulerProvider getSchedulerProvider() {
        return schedulerProvider;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        super.onCleared();
    }

    public boolean isLoading() {
        return isLoading.get();
    }

    public void setLoading(boolean isLoading) {
        this.isLoading.set(isLoading);
    }
}
