package com.nmgtechnologies.mvvm.ui.base;

import android.arch.lifecycle.ViewModel;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public abstract class BaseRecyclerAdapter<U,T extends ViewModel,V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {
    public abstract List<T> toViewModelList(List<U> objectList);
    }
