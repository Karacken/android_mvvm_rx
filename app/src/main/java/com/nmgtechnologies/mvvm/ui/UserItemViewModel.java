package com.nmgtechnologies.mvvm.ui;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import com.nmgtechnologies.mvvm.data.model.api.User;

public class UserItemViewModel extends ViewModel {
    private final ObservableField<String> name;
    private final ObservableField<String> email;

    public UserItemViewModel(User user)
    {
        this.name=new ObservableField<>(user.getName());
        this.email=new ObservableField<>(user.getEmail());
    }

    public ObservableField<String> getName() {
        return name;
    }

    public ObservableField<String> getEmail() {
        return email;
    }
}
