package com.nmgtechnologies.mvvm.ui.base;

import android.arch.lifecycle.ViewModel;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import dagger.android.AndroidInjection;

public abstract class BaseActivity<T extends  ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity {
private T viewDataBinding;
private V viewModel=null;
public abstract @LayoutRes int getLayoutId();
public abstract int getBindingVariable();
public abstract V getViewModel();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        performDataBinding();

    }


    private void performDependencyInjection()
    {
        AndroidInjection.inject(this);
    }

    private void performDataBinding()
    {
        viewModel= viewModel==null ? getViewModel() : viewModel;
        viewDataBinding=DataBindingUtil.setContentView(this,getLayoutId());
        viewDataBinding.setVariable(getBindingVariable(),getViewModel());
        viewDataBinding.executePendingBindings();
    }

    public T getViewDataBinding() {
        return viewDataBinding;
    }
}
