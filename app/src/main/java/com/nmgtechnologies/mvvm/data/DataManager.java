package com.nmgtechnologies.mvvm.data;

import com.nmgtechnologies.mvvm.data.local.PreferenceHelper;
import com.nmgtechnologies.mvvm.data.remote.ApiHelper;

public interface DataManager extends ApiHelper,PreferenceHelper {

}
