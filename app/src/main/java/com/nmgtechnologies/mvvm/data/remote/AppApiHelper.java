package com.nmgtechnologies.mvvm.data.remote;

import com.nmgtechnologies.mvvm.data.model.api.User;
import com.nmgtechnologies.mvvm.data.model.api.GenericResponse;
import com.nmgtechnologies.mvvm.di.ApiKeyInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import retrofit2.Retrofit;

@Singleton
public class AppApiHelper implements ApiHelper{
    private AppApi appApi;
    private String apiKey;

    @Inject
    public AppApiHelper(Retrofit retrofit, @ApiKeyInfo String apiKey) {
        this.apiKey=apiKey;
        this.appApi =retrofit.create(AppApi.class);
    }

    @Override
    public Observable<GenericResponse<User>> getUserList() {
        return appApi.getUserList(apiKey);
    }
}
