package com.nmgtechnologies.mvvm.data.model.api;

import com.google.gson.annotations.Expose;

import java.util.List;

public class GenericResponse<T> {
    @Expose
    private int code;
    @Expose
    private String message;
    @Expose
    private List<T> data;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public List<T> getData() {
        return data;
    }
}
