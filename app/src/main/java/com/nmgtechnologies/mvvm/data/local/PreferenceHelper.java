package com.nmgtechnologies.mvvm.data.local;

public interface PreferenceHelper {

    String getAccessToken();
    Long getCurrentUserId();
    String getCurrentUserName();
    void setAccessToken(String token);
    void setCurrentUserId(Long userId);
    void setCurrentUserName(String name);
}
