package com.nmgtechnologies.mvvm.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.nmgtechnologies.mvvm.di.PrefNameInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.nmgtechnologies.mvvm.util.AppConstants.NULL_NUMBER;

@Singleton
public class AppPreferenceHelper implements PreferenceHelper {
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";

    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferenceHelper(Context context,@PrefNameInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getAccessToken() {
        return  mPrefs.getString(PREF_KEY_ACCESS_TOKEN,null);
    }

    @Override
    public Long getCurrentUserId() {
        long userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID,NULL_NUMBER);
        return userId == NULL_NUMBER ? null : userId;
    }

    @Override
    public String getCurrentUserName() {
        return  mPrefs.getString(PREF_KEY_CURRENT_USER_NAME,null);
    }

    @Override
    public void setAccessToken(String token) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, token).apply();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPrefs.edit().putLong(PREF_KEY_CURRENT_USER_ID, userId==null ? NULL_NUMBER : userId).apply();
    }

    @Override
    public void setCurrentUserName(String name) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, name).apply();
    }
}
