package com.nmgtechnologies.mvvm.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @Expose
    @SerializedName("id")
    String id;
    @Expose
    @SerializedName("name")
    String name;
    @Expose
    @SerializedName("email")
    String email;

    public User(int id, String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }


}
