package com.nmgtechnologies.mvvm.data.remote;
import com.nmgtechnologies.mvvm.data.model.api.User;
import com.nmgtechnologies.mvvm.data.model.api.GenericResponse;

import io.reactivex.Observable;
public interface ApiHelper {
    Observable<GenericResponse<User>> getUserList();
}
