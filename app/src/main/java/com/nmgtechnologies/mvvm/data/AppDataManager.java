package com.nmgtechnologies.mvvm.data;

import com.nmgtechnologies.mvvm.data.local.PreferenceHelper;
import com.nmgtechnologies.mvvm.data.model.Notification;
import com.nmgtechnologies.mvvm.data.model.api.User;
import com.nmgtechnologies.mvvm.data.model.api.GenericResponse;
import com.nmgtechnologies.mvvm.data.remote.ApiHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

@Singleton
public class AppDataManager implements DataManager {
    private ApiHelper apiHelper;
    private PreferenceHelper preferenceHelper;
    private PublishSubject<Object> dataBus = PublishSubject.create();
    @Inject
    public AppDataManager(ApiHelper apiHelper, PreferenceHelper preferenceHelper)
    {
        this.apiHelper=apiHelper;
        this.preferenceHelper=preferenceHelper;
    }

    @Override
    public String getAccessToken() {
        return preferenceHelper.getAccessToken();
    }

    @Override
    public Long getCurrentUserId() {
        return preferenceHelper.getCurrentUserId();
    }

    @Override
    public String getCurrentUserName() {
        return preferenceHelper.getCurrentUserName();
    }

    @Override
    public void setAccessToken(String token) {
        preferenceHelper.setAccessToken(token);
    }

    @Override
    public void setCurrentUserId(Long userId) {
        preferenceHelper.setCurrentUserId(userId);
    }

    @Override
    public void setCurrentUserName(String name) {
        preferenceHelper.setCurrentUserName(name);
    }

    @Override
    public Observable<GenericResponse<User>> getUserList() {
        return apiHelper.getUserList();
    }

    public Observable<Notification> notificationObservable()
    {
        return dataBus.map(o -> (Notification)o);
    }
    public void onNotificationReceived(Notification notification)
    {
        dataBus.onNext(notification);
    }
}
