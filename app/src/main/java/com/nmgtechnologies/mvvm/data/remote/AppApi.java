package com.nmgtechnologies.mvvm.data.remote;

import com.nmgtechnologies.mvvm.data.model.api.User;
import com.nmgtechnologies.mvvm.data.model.api.GenericResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface AppApi {

    @GET(ApiEndPoint.USER_LIST)
    Observable<GenericResponse<User>> getUserList(@Header("api_key")String apiKey);
}
