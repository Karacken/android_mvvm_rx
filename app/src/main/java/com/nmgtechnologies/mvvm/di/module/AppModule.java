package com.nmgtechnologies.mvvm.di.module;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nmgtechnologies.mvvm.BuildConfig;
import com.nmgtechnologies.mvvm.data.AppDataManager;
import com.nmgtechnologies.mvvm.data.DataManager;
import com.nmgtechnologies.mvvm.data.local.AppPreferenceHelper;
import com.nmgtechnologies.mvvm.data.local.PreferenceHelper;
import com.nmgtechnologies.mvvm.data.remote.ApiHelper;
import com.nmgtechnologies.mvvm.data.remote.AppApiHelper;
import com.nmgtechnologies.mvvm.di.ApiKeyInfo;
import com.nmgtechnologies.mvvm.di.BaseUrlInfo;
import com.nmgtechnologies.mvvm.di.PrefNameInfo;
import com.nmgtechnologies.mvvm.util.rx.AppSchedulerProvider;
import com.nmgtechnologies.mvvm.util.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @ApiKeyInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @PrefNameInfo
    String providePrefName() {
        return BuildConfig.PREF_NAME;
    }

    @Provides
    @BaseUrlInfo
    String provideBaseUrl() {
        return BuildConfig.BASE_URL;
    }

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }


    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    PreferenceHelper providePreferenceHelper(AppPreferenceHelper appPreferenceHelper) {
        return appPreferenceHelper;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    Gson provideGson()
    {
        return new GsonBuilder()
                .setLenient()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }


}
