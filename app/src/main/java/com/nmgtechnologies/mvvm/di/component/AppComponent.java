package com.nmgtechnologies.mvvm.di.component;

import android.app.Application;

import com.nmgtechnologies.mvvm.CleanApp;
import com.nmgtechnologies.mvvm.di.builder.ActivityBuilder;
import com.nmgtechnologies.mvvm.di.module.AppModule;
import com.nmgtechnologies.mvvm.di.module.RetrofitModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AppModule.class,RetrofitModule.class,AndroidInjectionModule.class,ActivityBuilder.class})
public interface AppComponent {
    void inject(CleanApp app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
