package com.nmgtechnologies.mvvm.di.module;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nmgtechnologies.mvvm.ViewModelProviderFactory;
import com.nmgtechnologies.mvvm.data.DataManager;
import com.nmgtechnologies.mvvm.ui.MainActivityViewModel;
import com.nmgtechnologies.mvvm.ui.UserListAdapter;
import com.nmgtechnologies.mvvm.util.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    public UserListAdapter provideUserListAdapter()
    {
        return new UserListAdapter(new ArrayList<>());
    }

    @Provides
    public MainActivityViewModel provideMainActivityViewModel(DataManager dataManager, SchedulerProvider schedulerProvider)
    {
        return new MainActivityViewModel(dataManager, schedulerProvider);
    }
    @Provides
    public ViewModelProvider.Factory provideViewModelFactory(MainActivityViewModel mainActivityViewModel)
    {
        return new ViewModelProviderFactory<>(mainActivityViewModel);
    }

    @Provides
    public LinearLayoutManager provideLinearLayoutManager(Context context)
    {
       return new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
    }

    @Provides
    public RecyclerView.ItemAnimator provideItemAnimator()
    {
        return new DefaultItemAnimator();
    }
}
