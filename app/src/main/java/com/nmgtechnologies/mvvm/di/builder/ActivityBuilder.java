package com.nmgtechnologies.mvvm.di.builder;

import com.nmgtechnologies.mvvm.di.module.MainActivityModule;
import com.nmgtechnologies.mvvm.ui.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();
}
